<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cathegory;
use App\User;

class CathegoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth')->except('index'); // aun sin logear podremos ver el listado.
    }

    public function index()
    {
        $this->authorize('index', User::class);
        $currentUser = \Auth::user();
        $categorias = Cathegory::paginate(10);
        return view('cathegory.index', ['categorias' => $categorias], ['currentUser' => $currentUser]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $currentUser = \Auth::user();
        if ($currentUser->can('create', Cathegory::class)) {
            return view('cathegory.create');
        }else{
            return back();
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validacion:
        $rules = [
            'name' => 'required|max:255|min:3',
        ];

        $request->validate($rules);

        $categoria = new Cathegory();
        $categoria->fill($request->all());
        $categoria->save();

        return redirect('/cathegories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cathegory = Cathegory::with('products')->findOrFail($id);
        // para recibir todos los productos pertenecientes a la categoría
        return view('cathegory.show', ['cathegory' => $cathegory]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      $currentUser = \Auth::user();
      if ($currentUser->can('edit', Cathegory::class)) {
       $categoria = Cathegory::findOrFail($id);
       return view('cathegory.edit', ['cathegory' => $categoria]);
   }else{
       return back();
   }
}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

     $rules = [
        'name' => 'required|max:255|min:3',

    ];

    $request->validate($rules);

    $categoria = Cathegory::findOrFail($id);
    $categoria->fill($request->all());
    $categoria->save();


    return redirect('/cathegories/' . $categoria->id);
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cathegory = Cathegory::findOrFail($id);
        $this->authorize('delete', $cathegory);

        if  (count($cathegory->products) != 0 ){
           return response()->view('errors.usedCathegory', ['cathegory' => $cathegory]);
        }

        $cathegory->delete();
        return back();
    }
}
